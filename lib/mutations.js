"use strict";
const connectDb = require("./db");
const { ObjectID } = require("mongodb");
const errorHandler = require("./errorHandler");
const { checkValue } = require("../utils/utils");
const bcrypt = require("bcrypt");

const saltRounds = 10;
const myPlaintextPassword = "s0//P4$$w0rD";
const someOtherPlaintextPassword = "not_bacon";

module.exports = {
  createTrayecto: async (root, { input }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newTrayecto = { ...input, fecha };
    let db;
    let trayectos;

    try {
      db = await connectDb();
      trayectos = await db.collection("trayectos").insertOne(newTrayecto);
      newTrayecto._id = trayectos.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newTrayecto;
  },
  createViaje: async (root, { idInicio, input }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newTrayecto = { ...input, fecha };
    try {
      let db = await connectDb();
      let trayectoVuelta = await db
        .collection("trayectos")
        .insertOne(newTrayecto);
      let inicio = await db
        .collection("trayectos")
        .findOne({ _id: ObjectID(idInicio) });
      const viaje = {
        idInicio: inicio._id,
        idFin: trayectoVuelta.insertedId,
        km: newTrayecto.odometro - inicio.odometro,
        fecha,
      };

      const result = await db.collection("viajes").insertOne(viaje);
      viaje._id = result.insertedId;
      return viaje;
    } catch (error) {
      errorHandler(error);
    }
  },
  createRepostaje: async (root, { input }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newRepostaje = { ...input, fecha };
    let db;
    let repostaje;
    try {
      db = await connectDb();
      const ultimoReportaje = await db
        .collection("repostajes")
        .find({})
        .sort({ _id: -1 })
        .limit(1)
        .toArray();

      if (ultimoReportaje.length > 0) {
        newRepostaje.km = newRepostaje.odometro - ultimoReportaje[0].odometro;
      }
      repostaje = await db.collection("repostajes").insertOne(newRepostaje);
      newRepostaje._id = repostaje.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newRepostaje;
  },
  createGasto: async (root, { concepto, tipo, cantidad, pagado }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newGasto = { concepto, tipo, cantidad, pagado, fecha };
    let db;
    let gasto;
    try {
      db = await connectDb();
      gasto = await db.collection("gastos").insertOne(newGasto);
      newGasto._id = gasto.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newGasto;
  },
  createUsuario: async (root, { nombre, apellidos, usuario, email, pass }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newUsuario = {
      nombre,
      apellidos,
      email,
      usuario,
      pass,
      dateCreate: fecha,
    };
    let db;
    let uausrio;
    try {
      db = await connectDb();
      uausrio = await db.collection("usuarios").insertOne(newUsuario);
      newUsuario._id = uausrio.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newUsuario;
  },

  /*
    Login
  */
  login: async (root, { email, pass }) => {
    const compruebaEmail = checkValue(email);
    const compruebaPass = checkValue(pass);

    if (!compruebaEmail || !compruebaPass) {
      return null;
    }
    let db;
    let token;
    let dateExpire;

    try {
      db = await connectDb();
      const usuario = await db.collection("usuarios").findOne({ email, pass });
      if (!usuario) {
        return null;
      }
      dateExpire = new Date();
      dateExpire.setDate(dateExpire.getDate() + 20);
      dateExpire = dateExpire.toISOString();

      const key = `${myPlaintextPassword}${dateExpire}`;
      token = await bcrypt
        .hash(myPlaintextPassword, saltRounds)
        .then((hash) => hash);
      console.log(token);
      await db.collection("sesiones").findOneAndUpdate(
        { user: usuario._id },
        {
          $setOnInsert: { user: usuario._id },
          $push: {
            sesions: { token, dateExpire },
          },
        },
        { upsert: true }
      );
    } catch (error) {
      errorHandler(error);
    }
    return { token, dateExpire };
  },

  createCasa: async (root, { nombre }) => {
    let db;
    let casa;
    let user = "a";
    const newCasa = {
      nombre,
      dateCreate: new Date().toLocaleString("es-ES"),
      integrantes: [user],
    };
    try {
      db = await connectDb();
      casa = await db.collection("casas").insertOne(newCasa);
      newCasa._id = casa.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newCasa;
  },
};
