"use strict";

const connectDb = require("./db");
const { ObjectID } = require("mongodb");
const errorHandler = require("./errorHandler");

module.exports = {
  getTrayectos: async () => {
    let db;
    let trayectos = [];

    try {
      db = await connectDb();
      trayectos = await db.collection("trayectos").find().toArray();
    } catch (error) {
      errorHandler(error);
    }

    return trayectos;
  },
  getTayectoInicio: async () => {
    let db;
    let trayecto;
    try {
      db = await connectDb();
      trayecto = await db
        .collection("trayectos")
        .find({})
        .sort({ _id: -1 })
        .limit(1)
        .toArray();
    } catch (error) {
      errorHandler(error);
    }
    if (trayecto && trayecto[0] && trayecto[0].trayecto === "inicio")
      return { OK: true, trayecto: trayecto[0] };
    else return { OK: false, trayecto: null };
  },
  getViajes: async () => {
    let db;
    let viajes = [];
    try {
      db = await connectDb();
      viajes = await db.collection("viajes").find().toArray();
    } catch (error) {
      errorHandler(error);
    }
    return viajes;
  },
  getRepostajes: async () => {
    let db;
    let repostajes = [];
    try {
      db = await connectDb();
      repostajes = await db.collection("repostajes").find().toArray();
    } catch (error) {
      errorHandler(error);
    }
    return repostajes;
  },
  getGastos: async () => {
    let db;
    let gastos = [];
    try {
      db = await connectDb();
      gastos = await db.collection("gastos").find().toArray();
    } catch (error) {
      errorHandler(error);
    }
    return gastos;
  },
  getUsuario: async (_, { email }) => {
    let db;
    let usuario = [];
    try {
      db = await connectDb();
      usuario = await db.collection("usuarios").findOne(email).toArray();
    } catch (error) {
      errorHandler(error);
    }
    return usuario;
  },
  me: async (root, { token }) => {
    let db;
    let sesion;
    let fecha = new Date().toISOString();
    const opciones = {
      "sesions.token": token,
      "sesions.dateExpire": { $gt: fecha },
    };
    try {
      db = await connectDb();
      sesion = await db.collection("sesiones").findOne(opciones);
    } catch (error) {
      errorHandler(error);
    }
    if (!sesion) {
      return null;
    }
    const usuario = await db.collection("usuarios").findOne(
      { _id: ObjectID(sesion.user) },
      {
        projection: {
          _id: 1,
          nombre: 1,
          usuario: 1,
          apellidos: 1,
          email: 1,
          casa: 1,
        },
      }
    );
    return usuario;

    /*
    const sesions = [];
    sesion.sesions.forEach((item) => {
      console.log(item.token === token);
      if (item.token === token) {
        let tokenFecha = new Date(item.dateExpire);
        tokenFecha.setDate(tokenFecha.getDate() + 5);
        tokenFecha = tokenFecha.toISOString();
        sesions.push({ token: item.token, dateExpire: tokenFecha });
      } else if (item.dateExpire > fecha) {
        sesions.push(item);
      }
    });
    */
    // sesion = await db.collection("sesiones").findOneAndUpdate(opciones);
  },
};
