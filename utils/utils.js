const connectDb = require("../lib/db");

module.exports = {
  checkValue: (value) => {
    if (typeof value === "string") {
      return value !== "";
    }
    if (typeof value === "number" && (value === 0 || value)) return true;
    return false;
  },
  activeSecion: async (token) => {
    if (!checkValue(token)) return null;
    let db;
    const sesiones = await db.collection("sesiones").findOne({
      "sesions.token": token,
      "sesions.dateExpire": { $gt: new Date() },
    });
    console.log(sesiones);
    return null;
  },
};
